package com.SDACourse.TDD;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureConverterTest {
    TemperatureConverter temperatureConverter = new TemperatureConverter();

    @Test
    public void shouldRetur67Fahrenheits() {
        // when
        double result = temperatureConverter.celsiusToFahrnenheit(19.44);
        // then
        assertEquals(67, result, 2);
    }

    @Test
    public void shouldReturn50Celsius() {
        double result = temperatureConverter.fahrenheitToCelsius(122);
        // then
        assertEquals(50, result, 2);
    }

    @Test
    public void shouldReturnMinus20Celsius() {
        double result = temperatureConverter.fahrenheitToCelsius(122);
        // then
        assertEquals(50, result, 2);
    }

    @Test
    public void shouldThrowException() {

        assertThrows(IllegalArgumentException.class, () -> temperatureConverter.celsiusToFahrnenheit(-277));

    }

    @Test
    public void shouldThrowExceptionFahrenheitEdition() {

        assertThrows(IllegalArgumentException.class, () -> temperatureConverter.fahrenheitToCelsius(-460));

    }
}