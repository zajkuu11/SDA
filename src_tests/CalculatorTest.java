import com.SDACourse.TDD.Calculator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private Calculator calculator;

    @BeforeAll
    @Disabled
    public static void setUp() {
        System.out.println("Before All");
    }

    @BeforeEach
    @Disabled
    public void setUpEach() {
        System.out.println("Before Each");
        calculator = new Calculator();
    }

    @AfterEach
    @Disabled
    public void tearDown() {
        System.out.println("After Each");
    }

    @Test
    public void shouldReturn50WhenAdding20And30() {
        // given
        // when
        int result = calculator.add(20, 30);
        // then
        assertEquals(50, result);
    }

    @Test
    public void shouldReturn40WhenAdding10And30() {
        // given
        // when
        int result = calculator.add(10, 30);
        // then
        assertEquals(40, result);
    }

    @Test
    public void shouldReturn60WhenAdding30And30() {
        // given
        // when
        int result = calculator.add(30, 30);
        // then
        assertEquals(60, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"RED", "GREEN", "BLACK", "BLUE", "ORANGE"})
    public void shouldTestIfColorIsABaseColor(String color) {
        Map<String, Boolean> baseColors = new HashMap<>();
        baseColors.put("RED", true);
        baseColors.put("BLUE", true);
        baseColors.put("GREEN", true);


        assertTrue(baseColors.getOrDefault(color, false));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    public void testWithValueSource(int argument) {
        assertTrue(argument > 0 && argument < 4);
    }

    @ParameterizedTest
    @MethodSource("stringProvider")
    void testWithSimpleMethodSource(String argument) {

        assertNotNull(argument);
    }

    static Stream<String> stringProvider() {
        return Stream.of("foo", "bar");
    }

    @ParameterizedTest
    @CsvSource({"hak,1", "bar,2", "baz,3"})
    void testWithCSVSource(String first, int second) {
        assertNotNull(first);
        assertNotEquals(0, second);
    }

    @Test
    @EnabledOnOs({OS.LINUX, OS.MAC})
    void shouldRunOnLinuxORMac() {
        assertEquals("Hello from Linux", "what?");
    }

    @Test
    @EnabledOnJre(JRE.JAVA_9)
    void shouldRunOnJava9() {
        assertEquals("is it java 9?", "no it's 8");
    }

    @Test
    public void shouldParseValidNumber() {
        // given

        // when
        int result = calculator.parse("100");
        // then
        assertEquals(100, result);
    }

    @Test
    public void shouldThrowNumberFormatException() {

        assertThrows(NumberFormatException.class, () -> calculator.parse("asdf"));
    }

    @Test
    public void shouldThrowRightMessageExcepetion() {

        NumberFormatException result = assertThrows(NumberFormatException.class, () -> calculator.parse("gfdsgd"));
        assertEquals("Niepoprawny numer", result.getMessage());
    }

}