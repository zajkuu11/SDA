package com.SDACourse.algorithms;

public class Box<T> {
    private T element;
    public Box (T element){
        this.element = element;
    }

    public T getElement(){
        return element;
    }


    public static void main(String[] args) {

        Box<Person> osoba = new Box<>(new Person("adam", 10));
        System.out.println(osoba.toString());
    }
}
