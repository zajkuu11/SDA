package com.SDACourse.algorithms;

public class Sort {
    static void sort(int[] arr) {

        int[] sortedArray = new int[arr.length];
        int min;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length-1; j++) {
                if (arr[j] > arr[j+1]){
                    min = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = min;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5, 8, 2, 3, 10};
        sort(arr);
    }
}
