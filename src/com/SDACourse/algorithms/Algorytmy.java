package com.SDACourse.algorithms;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Algorytmy extends SzyfrCezara{
    static int divisorSum(int value) {
        int sum = 0;
        int divisor = value;
        while (divisor > 0) {
            if (value % divisor == 0) {
                sum = sum + (value / divisor);
            }
            divisor--;
        }

        return sum;
    }

    static void primaryNumber(int value) {
        boolean isTrue = false;
        if (value <= 3) {
            isTrue = true;
        } else {
            int divisor = value / 2;
            while (divisor > 1) {
                if (value % divisor == 0) {
                    isTrue = false;
                    break;
                } else isTrue = true;
                divisor--;
            }
        }
        if (isTrue) {
            System.out.println(value + " jest pierwsza");
        } else System.out.println(value + " nie jest pierwsza");
    }

    static int silnia(int n) {
        if (n == 0 || n == 1)
            return 1;
        else return n * silnia(n - 1);
    }

    static int fibonacci(int n) {
        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }

    static void ciag(int n) {
        int expr1 = 1;
        for (int i = 0; i < n; i++) {
            System.out.print(expr1 + " ");
            expr1 += 2;
        }
    }

    static void ciag2(int n) {
        int expr1 = -1;
        int expr = 0;
        for (int i = 0; i < n; i++) {
            System.out.print(expr1 + " ");
            expr1 = expr1 * 2 + 3;
        }
    }

    static void ciag3(int n) {
        int expr1 = 2;
        boolean isTrue = true;
        for (int i = 0; i < n; i++) {
            System.out.print(expr1 + " ");
            if (isTrue) {
                expr1 += 2;
                if (expr1 == 8) {
                    isTrue = false;
                }
            } else {
                expr1 -= 2;
                if (expr1 == 2) {
                    isTrue = true;
                }
            }
        }
    }

    static void trojkat(int h) {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    static void przekatna(int w) {
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }
    }

    static void prostokat(int h, int w) {

        for (int i = 0; i < w; i++) {
            System.out.print("*");
        }
        System.out.println();
        for (int i = 0; i < h - 2; i++) {
            System.out.print("*");
            for (int j = 0; j < w - 2; j++) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }
        for (int i = 0; i < w; i++) {
            System.out.print("*");
        }
    }


    static void iks(int h) {
        char[][] arr = new char[h][h];
        int counter = arr.length - 1;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                arr[i][j] = ' ';
            }
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = '*';
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i][counter] = '*';
            counter--;
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    static void fileReader() throws Exception {
        Scanner scanner = new Scanner(Paths.get("liczby.txt"));
        List<Integer> liczby = new ArrayList<Integer>();
        int sum = 0;
        while (scanner.hasNext()) {
            liczby.add(scanner.nextInt());
        }
        for (Integer integer : liczby) {
            sum += integer;
            System.out.println(sum);
        }
    }

    static List<String> palindromReadData() throws IOException {
        Scanner scanner = new Scanner(Paths.get("palindrom.txt"));
        List<String> lista = new ArrayList<>();
        while (scanner.hasNext()) {
            lista.add(scanner.nextLine());
        }
        return lista;
    }

    static void isPalindrom(List<String> list) {
        boolean isTrue;
        for (String s : list) {
            int counter = s.length() - 1;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == s.charAt(counter--)) {
                    isTrue = true;
                    if (i <= counter) {
                        System.out.println(s + " jest palindromem");
                        break;
                    }
                } else {
                    isTrue = false;
                    System.out.println(s + " nie jest palindromem");
                    break;
                }
            }
        }
    }

    static void hajsik(double hajs) {
        while (hajs >= 0) {
            if (hajs >= 500) {
                hajs -= 500;
                System.out.println(500);
            } else if (hajs >= 200 && hajs < 499) {
                hajs -= 200;
                System.out.println(200);
            } else if (hajs >= 100 && hajs < 200) {
                hajs -= 100;
                System.out.println(100);
            } else if (hajs >= 50 && hajs < 100) {
                hajs -= 50;
                System.out.println(50);
            } else if (hajs >= 20 && hajs < 50) {
                hajs -= 20;
                System.out.println(20);
            } else if (hajs >= 10 && hajs < 20) {
                hajs -= 10;
                System.out.println(10);
            } else if (hajs >= 5 && hajs < 10) {
                hajs -= 5;
                System.out.println(5);
            } else if (hajs >= 2 && hajs < 5) {
                hajs -= 2;
                System.out.println(2);
            } else if (hajs >= 1 && hajs < 2) {
                hajs -= 1;
                System.out.println(1);
            } else if (hajs >= 0.5 && hajs < 1) {
                hajs -= 0.50;
                System.out.println(0.50);
            } else if (hajs >= 0.2 && hajs < 0.5) {
                hajs -= 0.20;
                System.out.println(0.20);
            } else if (hajs >= 0.1 && hajs < 0.2) {
                hajs -= 0.10;
                System.out.println(0.10);
            } else if (hajs >= 0.05 && hajs < 0.1) {
                hajs -= 0.05;
                System.out.println(0.05);
            } else if (hajs >= 0.02 && hajs < 0.05) {
                hajs -= 0.02;
                System.out.println(0.02);
            } else {
                hajs -= 0.01;
                System.out.println(0.01);
            }
        }
    }

    static void pattern(String text, String pattern){
        for (int i = 0; i < text.length(); i++) {
            if (i <= text.length() - pattern.length()){
                String temp = text.substring(i, (pattern.length() + i));
                if (temp.contains(pattern)){
                    System.out.println(i + " " + (pattern.length() + i ));
                }
            }
        }
    }

    static void findPalindromes(){
        String palindrome = "bcaccaabab";
        int counter= palindrome.length()-1;

    }

    public static void main(String[] args) throws Exception {

    }


}
