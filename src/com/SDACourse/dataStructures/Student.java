package com.SDACourse.dataStructures;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Student findStudent(List students, String name) {
        for (int i = 0; i < students.size(); i++) {
            if (((Student) students.get(i)).getName().equals(name)) {
                return (Student) students.get(i);
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        List students = new ArrayList();
        students.add(new Student("Jan Kowalski"));
        students.add(new Student("Andrzej Nowak"));
        Student student1 = new Student("Jarek");


        System.out.println(student1.findStudent(students, "Andrzej Nowak"));
    }
}
