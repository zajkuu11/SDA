package com.SDACourse.dataStructures.set;

import java.util.HashSet;
import java.util.Set;

public class Main {


    public static void main(String[] args) {
        Set<String> text = new HashSet<String>();
        text.add("tak");
        if (!text.add("tak")) {
            System.out.println("Obiekt juz istnieje");
        }
        System.out.println(text);


        Set<Object> obj = new HashSet<Object>();

        obj.add("TAK");
        obj.add("NIE");

        if(!obj.add("ttt")){
            System.out.println("Istnieje!");
        }
        System.out.println(obj);


    }
}
