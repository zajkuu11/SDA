package com.SDACourse.dataStructures;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


public class Main {
    public static void main(String[] args) {
        Map grades = new LinkedHashMap();

        grades.put("Matematyka", 5);
        grades.put("Historia", 3);
        grades.put("Polski", 3);
        grades.put("Fizyka", 3);

       Set keys = grades.keySet();
       grades.putIfAbsent("Historia", 5);

        for (Object key: keys) {
            System.out.println(key + " : " + grades.get(key));
        }

    }
}

