package com.SDACourse.abstractInheritanceOverrifing.Point;


public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void increment(){
        x++;
        y++;
    }
    public void setValue(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void print(){
        System.out.println("x = " + this.x);
        System.out.println("y = " + this.y);
    }

}
