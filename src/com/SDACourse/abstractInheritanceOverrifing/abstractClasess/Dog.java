package com.SDACourse.abstractInheritanceOverrifing.abstractClasess;

public class Dog extends Animals {

    public Dog(int numberOfLegs, String sound) {
        super(numberOfLegs, sound);
    }

    @Override
    public void move() {

        System.out.println("Dog is moving on " + numberOfLegs + " legs");
    }

    @Override
    public String sound() {

        return sound;
    }
}

