package com.SDACourse.abstractInheritanceOverrifing.abstractClasess;

public abstract class Animals{

    protected int numberOfLegs;
    protected String sound;

    public Animals(int numberOfLegs, String sound) {
        this.numberOfLegs = numberOfLegs;
        this.sound = sound;
    }

    public abstract void move();
    public abstract String sound();
}
