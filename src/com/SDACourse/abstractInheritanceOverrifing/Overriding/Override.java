package com.SDACourse.abstractInheritanceOverrifing.Overriding;

public class Override {
    public int add(int a, int b) {
        System.out.println("int");
        return a + b;
    }

    public float add(float a, float b) {
        System.out.println("float");
        return a + b;
    }


    public int findMin(int first, int... others) {
        int min = 0;
        for (int i = 1; i < others.length; i++) {
            if (others[i] < first){
                min = others[i];
            }
            else min = first;

        }


        return min;
    }

}
