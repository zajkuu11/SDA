package com.SDACourse.abstractInheritanceOverrifing.lessonEight;


import java.util.Arrays;
import java.util.List;

public class EmployeesMap {

    public static void main(String[] args) {


        List<Employee> employee = Arrays.asList(
                new Employee("Adam", 35, 3500, 10),
                new Employee("Michal", 25, 4600, 10),
                new Employee("Michal", 37, 3500, 10),
                new Employee("Michal", 20, 3500, 10)
        );

        employee
                .stream()
                .filter(s -> s.age > 30)
                .filter(s -> s.salary < 4000)
                .forEach(System.out::println);
        System.out.println(employee
                .stream()
                .filter(s -> s.age > 30)
                .filter(s -> s.salary < 4000)
                .mapToDouble(s -> s.salary)
                .sum()
        );

    }
}

