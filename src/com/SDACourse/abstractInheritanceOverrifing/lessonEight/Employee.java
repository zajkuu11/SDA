package com.SDACourse.abstractInheritanceOverrifing.lessonEight;

public class Employee {
    String name;
    int age;
    float salary = 0;
    int seniority;

    public Employee(String name, int age, int salary, int seniority) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.seniority = seniority;
    }

    public Employee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSeniority() {
        return seniority;
    }

    public void setSeniority(int seniority) {
        this.seniority = seniority;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    float paySalary() {
        float seniorityBonus = 0;
        float ageBonus = 0;

        if (getSeniority() < 5) {
            seniorityBonus = 0;
        } else if (getSeniority() > 5 && getSeniority() <= 10) {
            seniorityBonus = 0.05f;
        } else if (getSeniority() > 10 && getSeniority() <= 15) {
            seniorityBonus = 0.1f;
        } else if (getSeniority() > 15) {
            seniorityBonus = 0.15f;
        }
        if (getAge() > 50 && getAge() <=60) {
            ageBonus = 0.05f;
        } else if (getAge() > 60 && getAge() <=70) {
            ageBonus = 0.10f;
        }
        salary = getSalary() * (1 + ageBonus + seniorityBonus);
        return salary;
    }

    public static void main(String[] args) {
        Employee employee = new Employee("jan", 51, 5000, 11);
        System.out.println(employee.paySalary());


    }
}
