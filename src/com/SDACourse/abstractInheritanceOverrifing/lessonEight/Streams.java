package com.SDACourse.abstractInheritanceOverrifing.lessonEight;

import java.util.Arrays;
import java.util.List;

public class Streams {
    public static void main(String[] args) {
        List<Integer> num = Arrays.asList(1, 3, 4, 5, 6, 10, 9, 7);

        num
                .stream()
                .filter(s -> s % 2 != 0)
                .map(s -> s * 100)
                .forEach(System.out::println);
    }
}
