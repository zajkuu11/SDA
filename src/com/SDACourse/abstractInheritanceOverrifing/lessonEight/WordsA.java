package com.SDACourse.abstractInheritanceOverrifing.lessonEight;

import java.util.Arrays;
import java.util.List;

public class WordsA {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("ala", "samochod", "kot", "aleksandra");

        list
                .stream()
                .filter(s -> s.startsWith("a"))
                .map(s -> {
                    String str = s.substring(0,1).toUpperCase();
                    String str2 = s.substring(1, s.length());
                    return str + str2;
                })
                .forEach(System.out::println);
    }
}
