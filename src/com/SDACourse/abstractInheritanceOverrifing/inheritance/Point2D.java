package com.SDACourse.abstractInheritanceOverrifing.inheritance;

public class Point2D {
    protected int x;
    protected int y;

    public Point2D() {
    }

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
