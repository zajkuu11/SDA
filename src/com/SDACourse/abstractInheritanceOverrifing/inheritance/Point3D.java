package com.SDACourse.abstractInheritanceOverrifing.inheritance;

public class Point3D extends Point2D {
    protected int z;

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    protected void print() {
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("z = " + z);


    }
}
