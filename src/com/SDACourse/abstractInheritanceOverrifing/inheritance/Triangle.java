package com.SDACourse.abstractInheritanceOverrifing.inheritance;

public class Triangle extends Shape {
    private double h;
    private double a;

    public Triangle(int h, int a) {
        this.h = h;
        this.a = a;
    }

    @Override
    public double area() {
        return 1 / 2.0 * a * h;
    }
}
