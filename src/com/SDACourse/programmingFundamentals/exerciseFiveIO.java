package com.SDACourse.programmingFundamentals;

import java.util.Scanner;

public class exerciseFiveIO {
    public static void main(String[] args) {
        String name;
        Scanner scanner = new Scanner(System.in);
        name = scanner.nextLine();
        System.out.println("Hello " + name);

        int number;
        number = scanner.nextInt();
        System.out.println(number);
    }
}
