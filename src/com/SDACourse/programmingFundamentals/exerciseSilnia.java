package com.SDACourse.programmingFundamentals;

import java.math.BigInteger;
import java.util.Scanner;

public class exerciseSilnia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        int silnia = 1;
        BigInteger sil = new BigInteger("1");

        if (n == 0 || n == 1) {
            System.out.println(1);
        } else {
            while (n > 1) {
//                silnia=silnia *n;
                sil = sil.multiply(BigInteger.valueOf(n));
                n--;

            }
        }
        System.out.println(sil);
    }
}
