package com.SDACourse.programmingFundamentals.lessonThree;

public class ExerciseTab2D1 {
    public static void main(String[] args) {

        int tab[][] = new int[2][3];

        tab[0][0] = 0;
        tab[0][1] = 1;

        tab[0][2] = 2;
        tab[1][0] = 3;

        tab[1][1] = 4;
        tab[1][2] = 5;

        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {

                System.out.print("tab[" + i + "," + j + "] = " + tab[i][j] + " ");
                if (Math.abs(i - j) == 1) {
                    System.out.println();
                }


            }

        }
    }
}
