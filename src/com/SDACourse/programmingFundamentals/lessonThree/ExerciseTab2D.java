package com.SDACourse.programmingFundamentals.lessonThree;

public class ExerciseTab2D {
    public static void main(String[] args) {

        int kolumna = 5;
        int wiersz = 5;

        int[][] tab = new int[kolumna][wiersz];

        for (int i = 0; i < 5; i++) {
            tab[i][i] = 0;
        }
        for (int i = 0; i < 5; i++) {
            tab[i][2] = 1;
        }

        for (int i = 0; i < wiersz; i++) {
            for (int j = 0; j < kolumna; j++) {
                System.out.print(tab[j][i] + " ");
            }
            System.out.println();
        }
        System.out.println();

        for (int i = 0; i < wiersz; i++) {
            for (int j = 0; j < kolumna; j++) {
                tab[j][i] = 0;
            }
        }

        tab[1][1] = 1;
        tab[2][2] = 2;
        tab[3][3] = 3;
        tab[4][4] = 4;

        for (int i = 0; i < wiersz; i++) {
            for (int j = 0; j < kolumna; j++) {
                System.out.print(tab[j][i] + " ");
            }
            System.out.println();
        }

/*        System.out.print(tab[0][1] + " ");
        System.out.print(tab[1][1] + " ");
        System.out.print(tab[2][1] + " ");
        System.out.print(tab[3][1] + " ");
        System.out.print(tab[4][1] + " \n");

        System.out.print(tab[0][2] + " ");
        System.out.print(tab[1][2] + " ");
        System.out.print(tab[2][2] + " ");
        System.out.print(tab[3][2] + " ");
        System.out.print(tab[4][2] + " ");*/

    }
}
