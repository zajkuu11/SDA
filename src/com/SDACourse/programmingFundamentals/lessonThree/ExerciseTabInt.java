package com.SDACourse.programmingFundamentals.lessonThree;

import java.util.Scanner;

public class ExerciseTabInt {
    public static void main(String[] args) {
        System.out.println("Wprowadz n wyrazow");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int wyraz[] = new int[n];

        for (int i = 0; i < n; i++) {
            wyraz[i] = i;
        }
        int i = 0;
        while (i != n) {
            System.out.println(wyraz[i]);
            i++;
        }
    }
}
