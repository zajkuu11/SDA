package com.SDACourse.programmingFundamentals.lessonThree;

public class Vehicle {


    private int numberOfWheels;
    private String color;
    private int velocity = 100;

    /*public Vehicle(String color) {
        this.color = color;
    }*/

    public Vehicle(int numberOfWheels, String color) {
        this.numberOfWheels = numberOfWheels;
        this.color = color;
    }

    public Vehicle(int numberOfWheels, String color, int velocity) {
        this.numberOfWheels = numberOfWheels;
        this.color = color;
        this.velocity = velocity;
    }


    public static void main(String[] args) {
        /*Vehicle vehicle = new Vehicle("Red");
        System.out.println(vehicle.color);

        Vehicle vehicle1 = new Vehicle("Green");
        System.out.println(vehicle1.color);

        System.out.println(vehicle.a);
        System.out.println(vehicle1.a);*/

        Vehicle vehicle = new Vehicle(4, "red", 150);

        System.out.println("Pojad ma " + vehicle.numberOfWheels +  " kola, jest " + vehicle.color + " i moze jechac " + vehicle.velocity);

        Vehicle vehicle1 = new Vehicle(4, "red");
        System.out.println("Pojad ma " + vehicle1.numberOfWheels +  " kola, jest " + vehicle1.color + " i moze jechac " + vehicle1.velocity);
    }
}
