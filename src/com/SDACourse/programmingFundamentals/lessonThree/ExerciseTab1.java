package com.SDACourse.programmingFundamentals.lessonThree;

public class ExerciseTab1 {
    public static void main(String[] args) {
        int tab[] = new int[]{1, 3, 5, 10};
        System.out.println("Wszystkie elementy po kolei");
        for (int i = 0; i < 4; i++){
            System.out.println(tab[i]);
        }
        System.out.println("Tylko parzysty index");
        for(int i =0; i<4; i+=2){
            System.out.println(tab[i]);
        }
        System.out.println("Tylko liczby parzyste");
        for(int i=0; i<4; i++){
            if(tab[i]%2==0){
                System.out.println(tab[i]);
            }
        }
        System.out.println("Odwrocona kolejnosc elementow");
        for(int i=3; i>=0; i--){
            System.out.println(tab[i]);
        }

    }
}
