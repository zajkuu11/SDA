package com.SDACourse.programmingFundamentals.lessonThree;

import java.io.*;
import java.util.Scanner;

public class ExerciseExternalFile {
    public static void main(String[] args) throws IOException {

        System.out.print("Podaj swoje imie ");

        File file = new File("plik.txt");
        PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", true));
        Scanner scanner = new Scanner(System.in);
        String imie = scanner.nextLine();
        writer.println(imie);
        writer.close();
        scanner.close();

        Scanner scanner1 = new Scanner(new File("plik.txt"));
        while (scanner1.hasNext()){
            System.out.println(scanner1.nextLine());
        }






    }
}
