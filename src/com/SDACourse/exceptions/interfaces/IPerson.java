package com.SDACourse.exceptions.interfaces;

public interface IPerson {
    String firstName();
    String lastName();
}
