package com.SDACourse.exceptions.interfaces;

public interface Iemployee {

    Employer getEmployer();
    double calcSalary();
}
