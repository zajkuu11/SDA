package com.SDACourse.exceptions.interfaces;

public class Main {

    public static void main(String[] args) {

        Person person = new Person("tak", "nie");

        System.out.println(person.firstName());
        System.out.println(person.lastName());

        System.out.println(person.toString());
    }
}
