package com.SDACourse.exceptions.interfaces;

public class Employee implements Iemployee, IPerson{

    Employer employer;
    String firstName;
    String lastName;
    int salary;

    public Employee(Employer employer, String firstName, String lastName, int salary) {
        this.employer = employer;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    @Override
    public Employer getEmployer() {
        return employer;
    }

    @Override
    public double calcSalary() {
        System.out.print("Wyplata: ");
        return salary;
    }


    @Override
    public String firstName() {
        return firstName;
    }

    @Override
    public String lastName() {
        return lastName;
    }


    public static void main(String[] args) {

        Employee employee = new Employee(new Employer("Adam", "Kowalski", "Żabka"), "Michal", "Nowak", 5000);
        System.out.println(employee.getEmployer());
        System.out.println(employee.calcSalary());

    }
}
