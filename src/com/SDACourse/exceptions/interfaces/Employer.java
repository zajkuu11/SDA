package com.SDACourse.exceptions.interfaces;

public class Employer extends Person implements Iemployer{

    String company;

    public Employer(String firstName, String lastName, String company) {
        super(firstName, lastName);
        this.company = company;
    }

    @Override
    public String getCompany() {
        return company;
    }

    @Override
    public String toString() {
        return
                "First Name = " + firstName + " \nLast Name = " + lastName  + " \nCompany = " + company;
    }

}
