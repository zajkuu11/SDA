package com.SDACourse.exceptions.exceptions;

import java.io.*;
import java.util.Scanner;

public class fileReadExceptions {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj sciezke do pliku");
        String filePath = scanner.nextLine();

        try {

            File file = new File(filePath);
            BufferedReader fileReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = fileReader.readLine()) != null) {
                System.out.println(line);
            }
        }
        catch (FileNotFoundException e){
            System.out.println("Brak takiego pliku! " + filePath);
        }
        catch (IOException e){
            System.out.println("Blad wejscia/wyjscia");
        }
    }
}
