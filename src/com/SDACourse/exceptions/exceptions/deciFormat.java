package com.SDACourse.exceptions.exceptions;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

public class deciFormat {

    public static void main(String[] args) {

        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols instance = DecimalFormatSymbols.
                getInstance(new Locale("en", "US"));
        decimalFormat.setDecimalFormatSymbols(instance);

        try {
            System.out.println(decimalFormat.parse("123.22"));
        } catch (ParseException e) {
            System.out.println("Zly format");
            e.printStackTrace();
        }
    }
}
