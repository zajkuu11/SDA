package com.SDACourse.exceptions;

public class Text implements IcharSequence {
    String str;
    int beginIndex;
    int endIndex;
    String reverse;


    public Text(String str, int beginIndex, int endIndex) {
        this.str = str;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    @Override
    public String str() {
        str.substring(beginIndex, endIndex);
        for (int i = endIndex; i >= beginIndex; i--) {
            reverse += str.charAt(i);
        }
        return str;
    }

}


