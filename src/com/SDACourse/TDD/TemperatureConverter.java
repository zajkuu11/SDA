package com.SDACourse.TDD;

public class TemperatureConverter {

    public double celsiusToFahrnenheit(double celsjusz) throws IllegalArgumentException {
        if (celsjusz < -273.15) {
            throw new IllegalArgumentException("Absolutne zero");
        }
        return (celsjusz * 1.8) + 32;

    }

    public double fahrenheitToCelsius(double fahrenheit) throws IllegalArgumentException {
        if (fahrenheit < -459.67) {
            throw new IllegalArgumentException("Absolutne zero");
        }
        return (fahrenheit - 32) / 1.8;
    }

}
