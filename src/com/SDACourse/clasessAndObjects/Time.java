package com.SDACourse.clasessAndObjects;

public class Time {
    private int godziny;
    private int minuty;

    public Time(int godziny, int minuty) {
        this.godziny = godziny;
        this.minuty = minuty;
    }

    private void add(Time time1) {
        System.out.print(this.godziny + "h " + this.minuty + "min" + " + " + time1.godziny + "h " + time1.minuty + "min" + " = ");
        this.godziny += time1.godziny;
        this.minuty += time1.minuty;

        if (this.minuty > 59) {
            this.godziny++;
            this.minuty = this.minuty % 60;
        }
        System.out.println(this.godziny + "h " + this.minuty + "min");
    }
    private void substract(Time time2){
        System.out.print(this.godziny + "h " + this.minuty + "min" + " - " + time2.godziny + "h " + time2.minuty + "min" + " = ");
        this.godziny -= time2.godziny;
        this.minuty -= time2.minuty;
        if (this.minuty < 0) {
            this.godziny--;
            this.minuty = this.minuty % 60;
        }
        System.out.println(this.godziny + "h " + this.minuty + "min");
    }

    public static void main(String[] args) {
        Time time = new Time(2, 30);
        time.add(new Time(5, 56));
        time.substract(new Time(1,40));


    }


}
