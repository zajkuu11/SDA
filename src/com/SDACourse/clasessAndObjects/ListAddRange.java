package com.SDACourse.clasessAndObjects;

public class ListAddRange extends List {

    public ListAddRange(int capacity) {
        super(capacity);
    }

    public void add(int value) {
        if (size >= capacity) {
            System.out.println("Lista pelna");
        } else if (value >= 0 && value <= 50) {
            numbers[size++] = value;
        } else System.out.println("Nie Wprowadzono liczby z zakresu 0-50");
    }
}
