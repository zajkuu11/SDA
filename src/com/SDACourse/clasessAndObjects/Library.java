package com.SDACourse.clasessAndObjects;



public class Library {
    private Movie[] movies;


    public static void main(String[] args) {
        Library library = new Library();
        library.movies = new Movie[]{new Movie(), new Movie("Rambo", 1985, "Action", "stallone" )};

        library.movies[0].setTitle("Titanic");
        library.movies[0].setDirector("James cameron");
        library.movies[0].setGenre("Drama");
        library.movies[0].setYear(1999);

        System.out.println(library.movies[0].getTitle() + library.movies[1].getTitle());



    }
}
