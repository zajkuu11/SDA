package com.SDACourse.clasessAndObjects;

class List {
    protected int[] numbers;
    protected int capacity;
    protected int size = 0;

    public List(int capacity) {
        this.capacity = capacity;
        this.numbers = new int[capacity];
    }

    public void add(int value) throws przekroczonoTablice {
        if (size >= numbers.length) {
            throw new przekroczonoTablice();
        } else
            numbers[size++] = value;
    }


    int get(int index) {
        int val = -1;

        if (index > size && index < capacity) {
            System.out.print("nie ma takiego indexu ");
        } else if (index > capacity) {
            System.out.print("poza zasiegiem ");
        }
        if (index <= capacity) {
            val = numbers[index];
        }
        return val;
    }

    void print() {
        for (int i = 0; i < capacity; i++) {
            if (i < size)
                System.out.print("[" + i + "] = " + numbers[i] + "; ");
        }
    }

    public static void main(String[] args){
        List list = new List(4);
        try {
            list.add(1);
            list.add(2);
            list.add(3);
            list.add(4);
            list.add(5);


        } catch (przekroczonoTablice e) {
            System.out.println("Tablica pelna");
        }

        list.print();

        System.out.println();
        System.out.println();

        ListAdd list1 = new ListAdd(5);
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(7);

        list1.print();

        System.out.println();

        ListAddRange list2 = new ListAddRange(5);
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(7);

        list2.print();


    }
}
