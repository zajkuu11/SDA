package com.hackerRank;

import java.util.Scanner;

public class PlusMinus {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int negative = 0;
        int positive = 0;
        int zero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                negative++;
            }
            else if (array[i] == 0){
                zero++;
            }
            else positive++;
        }

    }
}
