package hackerrank;

import java.util.Scanner;

public class loopsII {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int expr = 0;
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            expr = a;
            for (int j = 0; j < n; j++){
                expr = expr +  (int) Math.pow(2, j) * b;
                System.out.print(expr + " ");
            }
            System.out.println();
            expr = 0;
        }
        in.close();



    }
}
