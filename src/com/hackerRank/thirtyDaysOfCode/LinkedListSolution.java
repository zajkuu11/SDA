package hackerrank.thirtyDaysOfCode;

import java.util.LinkedList;
import java.util.Scanner;

public class LinkedListSolution {
    static int count=0;

    public static Node insert(Node head, int data) {
    LinkedList<Node> list = new LinkedList<Node>();

        //list.add(head, data);

    return head;
    }


    public static void display(Node head) {
        Node start = head;
        while (start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }



    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        Node head = null;
        int N = sc.nextInt();

        while (N-- > 0) {
            int ele = sc.nextInt();
            head = insert(head, ele);
        }
        display(head);
        sc.close();
    }
}
