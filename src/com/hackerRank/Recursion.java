package com.hackerRank;

import java.util.Scanner;

public class Recursion {
    static int factorial(int n) {
        if (n == 1 || n == 0) return 1;
        else return n * factorial(n - 1);

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int fac = scanner.nextInt();
        int result = factorial(fac);
        System.out.println(result);
    }
}
