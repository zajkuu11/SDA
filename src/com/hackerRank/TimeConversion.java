package com.hackerRank;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class TimeConversion {


    static String timeConversion(String s) {

        String hour = s.substring(0, 2);
        int h = Integer.parseInt(hour);
        if (s.charAt(8) == 'A') {
            if (h==12){
                return "0" + Integer.toString(Math.abs(h -= 12)) + s.substring(2, 8);
            }
            return "0" + Integer.toString(Math.abs(h)) + s.substring(2, 8);
        } else {
            if (h==12){
                return  Integer.toString(Math.abs(h)) + s.substring(2, 8);
            }
            return Integer.toString(h += 12) + s.substring(2, 8);
        }

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String s = scan.nextLine();

        String result = timeConversion(s);

        System.out.println(result);
    }
}
