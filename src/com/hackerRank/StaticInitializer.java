package com.hackerRank;

import java.util.Scanner;

public class StaticInitializer{
    static int B;
    static int H;
    static boolean flag;
    static{
        Scanner sc = new Scanner(System.in);
        B = sc.nextInt();
        H = sc.nextInt();
        try {
            if (B <= 0 || H <= 0) {
                flag = false;
                throw new Exception();
            }
            else flag=true;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Breadth and height must be positive");
        }


    }

    public static void main(String[] args) {

        if(flag){
            int area=B*H;
            System.out.print(area);
        }
    }
}
