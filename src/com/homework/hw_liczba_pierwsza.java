package com.homework;

import java.util.Scanner;

public class hw_liczba_pierwsza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean isPrime = true;

        for (int i=2; i<n; i++){
            int temp = n%i;
            if(temp==0){
                isPrime=false;
            }
        }
        if (isPrime){
            System.out.println("Jest liczba pierwsza");
        }
        else {
            System.out.println("Nie jest liczba pierwsza");
        }

    }
}

