package com.homework;

import org.mockito.Mock;

import java.util.Scanner;

public class FizzBuzz {
    public static void main(String[] args) {
        System.out.println("Do jakiej liczby gramy? ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            if (i % 3 != 0 && i % 5 != 0) {
                System.out.print(i + ", ");
            } else if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("Fizz Buzz, ");
            } else if (i % 3 == 0) {
                System.out.print("Fizz, ");
            } else if (i % 5 == 0) {
                System.out.print("Buzz, ");
            }


        }
    }

}
