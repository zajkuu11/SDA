package com.homework.fileMover;

import java.util.Map;

public interface Request {
    void handleRequest(Map data);
}
