package com.homework.bike;

public class MountainBike implements IBike {
    int gearsCount;
    int currentGear;

    public MountainBike(int gearsCount, int currentGear) {
        this.gearsCount = gearsCount;
        this.currentGear = currentGear;
    }

    @Override
    public boolean changeGear(int gear) throws UnsupportedOperationException {
        if (gear == 1 || gear == -1 && currentGear <= gearsCount) {
            currentGear += gear;
            return true;
        }
        if (currentGear > gearsCount || currentGear < 1) {
            throw new UnsupportedOperationException("Nie ma takich biegów");
        }
        return false;
    }

    @Override
    public int numOfGears() {
        return gearsCount;
    }

    @Override
    public int currentGear() {
        return currentGear;
    }
}
