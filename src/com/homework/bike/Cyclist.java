package com.homework.bike;

public class Cyclist implements IBike {
    IBike ibike;
    private Skladak skladak;
    private MountainBike mountainBike;
    private int gearsCount;
    private int currentGear;

    public Cyclist(IBike ibike) {
        this.ibike = ibike;
    }

    @Override
    public boolean changeGear(int gear) throws UnsupportedOperationException {
        if (ibike == skladak) {
            if (gear != 0) throw new UnsupportedOperationException("Skladak nie ma biegow");
            return false;
        } else {
            if (gear == 1 || gear == -1 && currentGear <= gearsCount) {
                currentGear += gear;
                return true;
            }
            if (currentGear > gearsCount || currentGear < 1) {
                throw new UnsupportedOperationException("Nie ma takich biegów");
            }
            return false;
        }
    }

    @Override
    public int numOfGears() {
        return gearsCount;
    }

    @Override
    public int currentGear() {
        return currentGear;
    }

    public static void main(String[] args) {
        Cyclist cyclist1 = new Cyclist(new MountainBike(6, 4));
        System.out.println(cyclist1.ibike.numOfGears());
        System.out.println(cyclist1.ibike.currentGear());

        Cyclist cyclist2 = new Cyclist(new Skladak(1, 1));
        System.out.println(cyclist2.ibike.currentGear());
    }
}
