package com.homework.bike;

public class Skladak implements IBike {
    int gearsCount;
    int currentGear;

    public Skladak(int gearsCount, int currentGear) {
        this.gearsCount = gearsCount;
        this.currentGear = currentGear;
    }

    @Override
    public boolean changeGear(int gear) throws UnsupportedOperationException {
        if (gear != 0) throw new UnsupportedOperationException("Skladak nie ma biegow");
        return false;
    }

    @Override
    public int numOfGears() {
        return currentGear;
    }

    @Override
    public int currentGear() {
        return currentGear;
    }

}
