package com.homework.bike;

public class Main {
    public static void main(String[] args) {
        MountainBike bike1 = new MountainBike(6, 1);
        bike1.changeGear(1);
        System.out.println(bike1.currentGear());
        MountainBike bike2 = new MountainBike(6, 6);
        bike2.changeGear(-1);
        System.out.println(bike2.currentGear);


        Skladak bike3 = new Skladak(1, 1);
        System.out.println(bike3.currentGear());
        bike3.changeGear(1);

    }
}
