package com.homework.bike;

public interface IBike {
    boolean changeGear(int gear);
    int numOfGears();
    int currentGear();
}
