package com.homework;

import java.util.Scanner;

public class Arrange {

    String text;

    public Arrange(String text) {
        this.text = text;
    }

    public String order(int order[]) {
        char[] chArray = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            chArray[i] = text.charAt(order[i]);
        }
        String a = new String(chArray);

        return a;
    }

    public static void main(String[] args) {
        Arrange text = new Arrange("Egzamin");
        System.out.print(text.order(new int[]{0, 5, 4, 3, 2, 1, 6}));

    }
}
