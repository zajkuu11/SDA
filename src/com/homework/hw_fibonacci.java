package com.homework;

import java.util.Scanner;

public class hw_fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int wyr0=0;
        int wyr1=1;

        for (int i = 2; i <= n; i++) {
            int suma = wyr0 + wyr1;
            wyr0 = wyr1;
            wyr1=suma;
            System.out.println(suma);
        }

    }
}
